import Foundation

//1) переробити калькулятор як клас з використанням enum
//2) в калькуляторі реалізувати збереження та вивід історії попередніх операцій

class Calculator {
    
    var operand1: Double = 0
    var operand2: Double = 0
    var history: [String] = []
    var result: Double = 0
    
    enum Operation: String {
        case addition = "+"
        case subtraction = "-"
        case multiplication = "*"
        case division = "/"
    }
    
    func calculate (operand1: Double, operand2: Double, operation: Operation) -> Double {
        //var result: Double = 0
        switch operation {
        case .addition :
            result = operand1 + operand2
            memory(a: operand1, b: operand2, c: .addition)
        case .subtraction :
            result = operand1 - operand2
            memory(a: operand1, b: operand2, c: .subtraction)
        case .multiplication :
            result = operand1 * operand2
            memory(a: operand1, b: operand2, c: .multiplication)
        case .division where operand2 != 0 :
            result = operand1 / operand2
            memory(a: operand1, b: operand2, c: .division)
        default:
            result = 0
        }
        
        return result
    }
    
    func memory (a: Double, b: Double, c: Operation) -> [String] {
        history.append("\(a) \(c.rawValue) \(b) = \(result)" )
        return history
    }
    
    func printHistory () {
        if history.count != 0 {
            for elem in history {
                print(elem)
            }
        } else {
            print("history is empty")
        }
    }
    
    init() {
    }
}

var math = Calculator()
math.calculate(operand1: 2, operand2: 4, operation: .multiplication)
math.history
math.calculate(operand1: 4, operand2: 3, operation: .subtraction)
math.history
math.calculate(operand1: math.calculate(operand1: 3, operand2: 7, operation: .addition), operand2: 5, operation: .division)
math.printHistory()


//3) Розробити три класи: коло, прямокутник та трикутник, написати функцію для
//обчислення площі, для трикутника та чотирикутника розробити конструктори для
//створення рівнобедренних трикутників, рівносторонніх та квадрата

func check (value: Double) -> Double {
    if value < 0 {
        print("Відємне значення не допустиме, встановлено в додатнє")
        return value * -1
    } else if value == 0 {
        print("Значення 0 не допустиме, змініть значення")
        return 0
    } else {
        return value
    }
}


class Circle {
    var radius: Double {
        didSet {
            self.radius = check(value: radius)
        }
    }
    
    init(radius: Double) {
       self.radius = check(value: radius)
    }
}


let circle1 = Circle(radius: -9)
let circle2 = Circle(radius: 0)
let circle3 = Circle(radius: 5)
circle1.radius
circle2.radius
circle3.radius
circle1.radius = -7
circle2.radius = 0
circle3.radius = 6
circle1.radius
circle2.radius
circle3.radius


class Rectangle {
    var height: Double {
        didSet {
           self.height = check(value: height)
        }
    }
    var width: Double {
        didSet {
            self.width = check(value: width)
        }
    }
    
    init (height: Double, width: Double) {
        self.height = check(value: height)
        self.width = check(value: width)
    }
    
    convenience init (heightAndWidth: Double) {
        self.init(height: heightAndWidth, width: heightAndWidth)
    }
    
    func area() -> Double {
        return height * width
    }
}



class Triangle {
    
    var isEquilateral: Bool?
    
    func comparison (left: Double, right: Double, bottom: Double) -> Bool {
        if left == right && left == bottom {
            isEquilateral = true
            return true
        } else {
            isEquilateral = false
            return false
        }
    }
    
    
    var leftSide: Double {
        didSet {
            self.leftSide = check(value: leftSide)
        }
    }
    
    var rightSide: Double {
        didSet {
           self.rightSide = check(value: rightSide)
        }
    }
    
    var bottomSide: Double {
        didSet {
        self.bottomSide = check(value: bottomSide)
            if (bottomSide >= rightSide + leftSide) || (bottomSide <= leftSide - rightSide) || (bottomSide <= rightSide - leftSide) {
                print("Не допустиме значення сторін")
            }
        }
    }
    
    init(leftSide: Double, rightSide: Double, bottomSide: Double) {
        self.leftSide = check(value: leftSide)
        self.rightSide = check(value: rightSide)
        self.bottomSide = check(value: bottomSide)
        if (bottomSide >= rightSide + leftSide) || (bottomSide <= leftSide - rightSide) || (bottomSide <= rightSide - leftSide) {
            print("Не допустиме значення сторін")
        }
    }
    
    convenience init(allSides: Double) {
        self.init(leftSide: allSides, rightSide: allSides, bottomSide: allSides)
    }
    
    convenience init(leftRightSides: Double, bottomSide: Double) {
        self.init(leftSide: leftRightSides, rightSide: leftRightSides, bottomSide: bottomSide)
    }
 
    func area() -> Double {
        let p = (leftSide + rightSide + bottomSide) / 2
        return sqrt(p * (p - leftSide) * (p - rightSide) * (p - bottomSide))
    }
    
    func triangleSides() {
        print("Сторони трикутника рівні \(leftSide) \(rightSide) \(bottomSide)")
    }
    
}

var triangle1 = Triangle(allSides: 4)
var triangle2 = Triangle(leftSide: 2, rightSide: 3, bottomSide: 6)
triangle2.bottomSide = 4
triangle2.bottomSide
var triangle3 = Triangle(leftRightSides: 3, bottomSide: -4)
triangle3.bottomSide
triangle3.bottomSide = 0
triangle3.bottomSide = -5
triangle3.bottomSide
triangle3.area()


// 
triangle1.isEquilateral
triangle1.comparison(left: 5, right: 5, bottom: 6)
triangle1.isEquilateral




triangle1.triangleSides()
triangle1.isEquilateral = true
