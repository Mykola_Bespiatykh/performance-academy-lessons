import Foundation

//  1) Написати програму для визначення чи число є парним з використання функцій

func isEven (number: Int) -> Bool {
    if number % 2 == 0 {
     return true
    } else {
        return false
    }
}

//func isOdd (number: Int) -> Bool {
//    if number % 2 != 0 {
//    return true
//    } else {
//        return false
//    }
//
//}

func isOdd (number: Int) -> Bool {
    return !isEven(number: number)
}


let n = 45
isEven(number: n)
isOdd(number: 45)


//  2) Вивести ряд Фібоначі з використанням функцій, функція своїм параметром має приймати кількість чисел рядку, які потрібно вивести, передбачити вивід помилки у випадку передачі у функцію хибного параметру


func fibonachi (countOfNumbers count: Int) {
    var number1 = 0
    var number2 = 1
    if count < 1 {
        print("Error")
    } else if count == 1 {
        print(number1)
    } else if count == 2 {
        print(number1)
        print(number2)
    } else {
        print(number1)
        print(number2)
        for _ in 0..<count-2 {
            let number = number1 + number2
            number1 = number2
            number2 = number
            print(number)
        }
    }
}

fibonachi(countOfNumbers: 7)



//   3) Написати функцію, яка буде приймати масив та повертати інший, що буде містити тільки непарні числа (використати функцію з першого завдання)


func oddNumbers (arrayOfNumbers: Array<Int>) -> Array<Int> {
    var oddNumbersArray: Array<Int> = []
    for element in arrayOfNumbers where isOdd(number: element) == true {
        oddNumbersArray.append(element)
    }
    return oddNumbersArray
}

let array1 = [5,8,7,87,65,54,21,77,6,2,76,34]
print(oddNumbers(arrayOfNumbers: array1))




//   5) Написати програму для визначення чи число просте (число,що ділиться тільки на 1 та на самого себе), з використанням функції для визначення кількості дільників. Розробити дві різні функції з використанням циклу while та for

func countOfDividers (number n: Int) -> Int {
    var count = 0
    for number in 1...n where n % number == 0 {
        count += 1

    }
    if count == 2 {
        print("Число \(n) просте")
    } else {
        print("Кількість дільників числа \(n) дорівнює \(count)")
    }
    return count
}



func countOfDividers1 (number n: Int) -> Int {
    var count = 0
    var number = 1
    while number <= n {
        if n % number == 0 {
            count += 1
        }
        number += 1
    }
    if count == 2 {
        print("Число \(n) просте")
    } else {
        print("Кількість дільників числа \(n) дорівнює \(count)")
    }
    return count
}

countOfDividers(number: 5)
countOfDividers1(number: 9)



//   4) Модернізувати калькулятор з попереднього уроку з використанням функцій (у функцію передавати два операнди та оператор у вигляді символу "-"."+". "/". "*")


let a: Double = 2
let b: Double = 3
let ab: Double = 10
func calculate (operand1 a: Double, operand2 b: Double, operator c: String) -> Double{
    var result : Double = 0
    if c == "/" && b == 0 {
        print("Помилка при діленні на 0")
    } else if c == "+" {
        result = a + b
    } else if c == "-" {
        result = a - b
    } else if c == "*" {
        result = a * b
    } else if c == "/" {
        result = a / b
    } else {
        print("Error")
    }
return result
    
}

calculate(operand1: a, operand2: b, operator: "+")
calculate(operand1: calculate(operand1: a, operand2: b, operator: "+") , operand2: ab, operator: "-")


//   Доробіть функцію universalCalc


func universalCalc (operands: Double..., operators: [String]) -> Double {
    if operands.count - 1 != operators.count {
        print("помилка - неправильна кількість операндів або операторів")
    } else {
        var result: Double = operands[0]
        for index in 1..<operands.count {
            result = calculate(operand1: result, operand2: operands[index], operator: operators[index - 1])
        }
        return result
    }
    return 0
}
universalCalc(operands: 1,2,3,4, operators: ["+","+","+","/"])
universalCalc(operands: 1,2,3,4,0, operators: ["+","+","+","/"])
print(universalCalc(operands: 1,2,3,4,5, operators: ["+","+","+","/"]))



//   Функцію реалізації решета Ератосфена (продвинутий та швидкий варіант визначення всіх простих чисел на проміжку). Функція приймає два параметри - цілі числа, початок та кінець проміжку


func findSimpleNumbersInRange (start: Int, end: Int) -> Array<Int> {
    var arrayOfNumbers: [Int] = []
    for i in start...end where i >= 2 {
        arrayOfNumbers.append(i)
    }
    var index = 0
    while index <= arrayOfNumbers.count - 1 {
        for number in arrayOfNumbers where (number % arrayOfNumbers[index] == 0) && (number != arrayOfNumbers[index]) {
            arrayOfNumbers.remove(at: arrayOfNumbers.firstIndex(of: number)!)
        }
        index += 1
    }
return arrayOfNumbers
}

print(findSimpleNumbersInRange(start: 1, end: 1000))
